# <img src="Images/plugin.png" height="30px" alt="Icon"> Convert Between Currencies
![Screendump](Images/CBC.jpg)   
This is a Wox plugin for converting currencies - e.g. "*24 eur in usd*" or just "*23 eur usd*"

## Installation in Wox
This repository must, according to Wox's documentation, be placed in ```<WoxDirectory>\Plugins\<YourPluginDirectory>``` - i.e. probably %USERPROFILE%\AppData\Roaming\Wox\Plugins (or so it is for me)

## Configuration config.ini
Some parameters are configurable in the *config.ini* file, as indicated below: 

### Convert Service
The "url" under section "UrlData" consists of three placeholders (e.g. http://valutaomregner.co/**[amount]**-**[src]**-**[dest]**)
- [amount] - the currency amount (24)
- [src] - the source currency (eur)
- [dest] - destination currency (usd)

### Document
The section "Document" holds two parameters, to select the HTML container with the converted value:
- containerTag is the tag-name of the container that holds the converted value
- containerClass - the tag's CSS class. 

As far as I've seen, the container has always a css class defined, and by these two parameters, it should be possible to pinpoint the required tag.

## Requirements
The plugin needs some Python modules 
- requests
- bs4 

Install with pip (e.g. pip install requests)

