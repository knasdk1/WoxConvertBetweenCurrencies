import requests
from wox import Wox, WoxAPI
from dependencies import clipboard
from os.path import dirname, join
import pycountry
from bs4 import BeautifulSoup
import re
import logging
import configparser

class Main(Wox):
	icon = join(dirname(__file__), 'Images', 'plugin.png')
	def query(self, query):
		results=[]
		
		self.config = configparser.ConfigParser()
		self.config._interpolation = configparser.ExtendedInterpolation()
		self.config.read('config.ini')
		self.debug = self.configSectionMap('System', 'debug')
		if self.debug == 'On':
			if self.configSectionMap('System', 'refreshLogOnEachRun') == 'On':
				fmode = 'w'
			else:
				fmode = 'a'
			logging.basicConfig(level=logging.DEBUG,
								format   ='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
								datefmt  = '%m-%d %H:%M',
								filename = self.configSectionMap('System', 'logFileName'),
								filemode = fmode)
			logging.getLogger("requests").setLevel(logging.WARNING)
			logging.getLogger("urllib3").setLevel(logging.WARNING)
		
		self.debugLine('Query from Wox: "' + query +'"')
		args = query.split(' ')

		if len(args) >= 3:
					
			currencyName = ''
			
			amount = args[0]
			## replace comma with dot
			amount = amount.replace(",", ".")
			self.debugLine('Query amount: ' + amount)

			# Pick up source currency
			fromCurrencyAbbr = args[1]
			
			if len(args[2]) > 2:
				toCurrencyAbbr = args[2]
			else:
				toCurrencyAbbr = args[3]
			self.debugLine('Query to currency: ' + toCurrencyAbbr)
			
			# Test if source currency is calid
			srcCurrencyAbbr = fromCurrencyAbbr.upper()
			try:
				self.getCurrency(srcCurrencyAbbr)
				self.debugLine('Source currency valid.')
				pass
			except Exception as e:
				self.debugLine('Source currency invalid.')
				return self.showErrorMessage(str(e))

			# Test if destination currency is valid
			destCurrencyAbbr = toCurrencyAbbr.upper()
			try:
				currencyName = self.getCurrency(destCurrencyAbbr)
				self.debugLine('Destination currency valid.')
				pass
			except Exception as e:
				self.debugLine('Destination currency invalid.')
				return self.showErrorMessage(str(e))
			
			# Call web converter
			#url = (self.configSectionMap('UrlData', 'url')) % (amount, fromCurrencyAbbr, toCurrencyAbbr)
			url = self.configSectionMap('UrlData', 'url').replace('[src]', fromCurrencyAbbr).replace('[dest]', toCurrencyAbbr).replace('[amount]', amount)
			self.debugLine('Calling ' + url + "\n")
			htmlContent = requests.get(url, verify=False)
			htmlParser = BeautifulSoup(htmlContent.text, "html.parser")
			
			# Find converted currency value
			containerTag = self.configSectionMap('Document', 'containerTag')
			containerClass = self.configSectionMap('Document', 'containerClass')
			currencyContent = htmlParser.find(containerTag, attrs={'class': containerClass})
			currencyValue = re.sub(r'[^\d,\.]', '', currencyContent.text)
			self.debugLine(currencyContent)
			self.debugLine('Found "' + currencyValue + '" with selectors "' + containerTag + '" -> "' + containerClass + '"')
						
			results.append({
				"Title": currencyValue +" "+ currencyName +"s",
 				"SubTitle":"Copy amount to clipboard",
 				"IcoPath":"Images/plugin.png",
 				"JsonRPCAction":{'method': 'copyToClipboard', 'parameters': [currencyValue],'dontHideAfterAction': False}
			})

			return results

	def debugLine(self, data):
		if self.debug == 'On':
			logging.debug(data)

	def copyToClipboard(self, data):
		clipboard.put(data)
		WoxAPI.show_msg("Value has been copied to clipboard.", '', self.icon)

	def getCurrency(self, currencyAbbr):
		try:
			CurrencyObject = pycountry.currencies.get(letter=currencyAbbr)
			return str(CurrencyObject.name)
		except Exception:
			raise Exception('"' + currencyAbbr + '" is not a valid currency abbrevation')

	def showErrorMessage(self, textMessage):
		results=[]
		results.append({
			"Title":"Currency Converter",
			"SubTitle":"Error: " + textMessage,
			"IcoPath":"Images/plugin.png"
		})
		return results

	def configSectionMap(self, section, param):
		#logging.error(self.config.get(section, param))
		paramVal = ''
		if self.config.has_section(section) and self.config.get(section, param):
			paramVal = self.config.get(section, param)
		else:
			paramVal = 'None'
		#logging.info(paramVal)
		return paramVal

if __name__ == "__main__":
	Main()